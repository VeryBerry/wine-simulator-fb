import firebase from 'firebase';

// firebase init goes here
const config = {
  apiKey: "AIzaSyBXy2VyjxdOVtklwRNUVBaZm36DfvT5TFE",
  authDomain: "wineecommerce-51468.firebaseapp.com",
  databaseURL: "https://wineecommerce-51468.firebaseio.com",
  projectId: "wineecommerce-51468",
  storageBucket: "wineecommerce-51468.appspot.com",
  messagingSenderId: "108121315752",
  appId: "1:108121315752:web:fe1d64e68eb052a161659d",
  measurementId: "G-81H8WPNE9V"
};

if (location.hostname === 'localhost') {
  config.databaseURL = `http://localhost:9000?ns=${config.projectId}`;
}

firebase.initializeApp(config);

// firebase utils
export const database = firebase.database();
const firestore = firebase.firestore();
/*
if (location.hostname === 'localhost') {
  firestore.settings({
    host: 'localhost:8080',
    ssl: false,
  });
  firebase.functions().useFunctionsEmulator('http://localhost:5001');
}
*/
export default firestore;
export const storage = firebase.storage();
export const auth = firebase.auth();
export const currentUser = auth.currentUser;
export const statesCollection = firestore.collection('states');
