import { Module } from 'vuex';
import { getters } from './state.getters';
import { actions } from './state.actions';
import { mutations } from './state.mutations';
import { StateState } from './state.types';
import { RootState } from '../../types';

export const state: StateState = {
  state: undefined,
};

const namespaced: boolean = true;

export const StateStore: Module<StateState, RootState> = {
  namespaced,
  state,
  getters,
  actions,
  mutations,
};
