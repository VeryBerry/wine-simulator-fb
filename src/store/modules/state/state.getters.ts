import { GetterTree } from 'vuex';
import { StateState, State } from './state.types';
import { RootState } from '../../types';

export const getters: GetterTree<StateState, RootState> = {
  state(storeState): State | undefined {
    const { state } = storeState;
    return state;
  },
};
