import { ActionTree } from 'vuex';
import { StateState, State } from './state.types';
import { RootState } from '../../types';
import { statesCollection } from '../../../firebase.config';

export const actions: ActionTree<StateState, RootState> = {

  add({ commit }, payload: { id: string, data: State }): Promise<void> {
    return statesCollection.doc(payload.id).set(payload.data).then((documentData) => {
      commit('added', documentData);
    }, (error) => {
      throw new Error(`state.actions.add(): ${error}`);
    });
  },

  get({ commit }, id: string): Promise<void> {
    return statesCollection.doc(id).get().then((documentData) => {
      if (documentData.exists) {
        commit('got', {
          id: documentData.id,
          ...documentData.data(),
        });
      }
    }, (error) => {
      throw new Error(`state.actions.get(): ${error}`);
    });
  },

  update({ commit }, update: { id: string, fields: any }): Promise<void> {
    return statesCollection.doc(update.id).update(update.fields).then(() => {
      commit('updated', update);
    }, (error) => {
      throw new Error(`state.actions.update(): ${error}`);
    });
  },

};
