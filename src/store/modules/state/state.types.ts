export interface State {
  id: string,
  mapJSON: string,
  currentMonth: number,
  currentGrapeYield: number,
}

export interface StateState {
  state?: State;
}
