import { MutationTree } from 'vuex';
import { StateState, State } from './state.types';

export const mutations: MutationTree<StateState> = {
  added(state, payload: State) {
    state.state = payload;
  },
  got(state, payload: State) {
    state.state = payload;
  },
  updated(state, payload: { id: string, fields: any }) {
    // state.wine = payload;
  },
};
