import { GetterTree } from 'vuex';
import { AuthState, Auth } from './auth.types';
import { RootState } from '../../types';

export const getters: GetterTree<AuthState, RootState> = {
  auth(state): Auth | undefined {
    const { auth } = state;
    return auth;
  },
};
