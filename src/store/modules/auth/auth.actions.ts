import { ActionTree } from 'vuex';
import { AuthState, Auth } from './auth.types';
import { RootState } from '../../types';

export const actions: ActionTree<AuthState, RootState> = {

  set({ commit }, auth: Auth): Promise<void> {
    return new Promise<void>(() => {
      commit('set', auth);
    });
  },

  unset({ commit }): Promise<void> {
    return new Promise<void>(() => {
      commit('unset');
    });
  },
};
