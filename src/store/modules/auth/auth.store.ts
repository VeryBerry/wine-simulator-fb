import { Module } from 'vuex';
import { getters } from './auth.getters';
import { actions } from './auth.actions';
import { mutations } from './auth.mutations';
import { AuthState } from './auth.types';
import { RootState } from '../../types';

export const state: AuthState = {
    auth: undefined,
};

const namespaced: boolean = true;

export const AuthStore: Module<AuthState, RootState> = {
    namespaced,
    state,
    getters,
    actions,
    mutations,
};
