import { MutationTree } from 'vuex';
import { AuthState, Auth } from './auth.types';

export const mutations: MutationTree<AuthState> = {
  set(state, payload: Auth) {
    state.auth = payload;
  },
  unset(state) {
    state.auth = undefined;
  },
};
