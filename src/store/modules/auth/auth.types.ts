export interface Auth {
  uid: string;
  displayName: string | null;
  email: string | null;
}

export interface AuthState {
  auth?: Auth;
}
