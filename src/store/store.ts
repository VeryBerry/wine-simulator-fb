import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { RootState } from './types';
import { AuthStore } from './modules/auth/auth.store';
import { StateStore } from './modules/state/state.store';

Vue.use(Vuex);

const state: RootState = {
  foo: 'bar',
};

const store: StoreOptions<RootState> = {
  state,
  modules: {
    auth: AuthStore,
    state: StateStore,
  },
};

export default new Vuex.Store<RootState>(store);
