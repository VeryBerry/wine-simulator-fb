import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/store';
import './registerServiceWorker';
import { __values } from 'tslib';
import './assets/theme.css';

Vue.config.productionTip = false;

import NavBar from './components/NavBar.vue';
import Footer from './components/Footer.vue';

Vue.component('NavBar', NavBar);
Vue.component('Footer', Footer);

import { auth } from './firebase.config';
import { Auth } from './store/modules/auth/auth.types';

let app: Vue;
auth.onAuthStateChanged((user: firebase.User | null) => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: (h) => h(App),
    }).$mount('#app');
  }

  if (user) {
    const payload: Auth = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
    };
    
    store.dispatch('auth/set', payload);
  }
});