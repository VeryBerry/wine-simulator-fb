import { Component, Vue } from 'vue-property-decorator';
import { Getter } from 'vuex-class';
import { Auth } from '@/store/modules/auth/auth.types';
import * as Products from '@/assets/products.json';

@Component({})
export default class Footer extends Vue {
  @Getter('auth', { namespace: 'auth' })
  public auth!: Auth;

  get products(): any[] {
    return Products.data;
  }

  get isAuthenticated(): boolean {
    return !!this.auth;
  }
}
