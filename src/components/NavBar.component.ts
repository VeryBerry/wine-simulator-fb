import { Component, Vue } from 'vue-property-decorator';
import { Getter } from 'vuex-class';
import { Auth } from '@/store/modules/auth/auth.types';
import * as Products from '@/assets/products.json';

@Component({})
export default class NavBar extends Vue {
  public isCollapsed: boolean = true;

  @Getter('auth', { namespace: 'auth' })
  public auth!: Auth;

  get products(): any[] {
    return Products.data;
  }

  get isAuthenticated(): boolean {
    return !!this.auth;
  }

  get welcomeMessage(): string {
    return this.auth.displayName || this.auth.email || '';
  }

  public onCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
  }
}
