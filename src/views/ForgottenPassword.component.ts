import { Component, Vue } from 'vue-property-decorator';
import { auth } from '@/firebase.config';

enum ButtonState {
  Send,
  Sending,
  Error,
}

@Component({})
export default class ForgottenPassword extends Vue {
  public email: string = '';
  public buttonState: ButtonState = ButtonState.Send;

  get backUrl(): string {
    return localStorage.getItem('backUrl') || '/';
  }

  get buttonText(): string {
    switch (this.buttonState) {
      case ButtonState.Send:
        return 'Send';
      case ButtonState.Sending:
        return 'Sending';
      case ButtonState.Error:
        return 'Please try again';
    }
  }

  get buttonClass(): string {
    switch (this.buttonState) {
      case ButtonState.Send:
        return 'btn-outline-primary';
      case ButtonState.Sending:
        return 'btn-primary';
      case ButtonState.Error:
        return 'btn-danger';
    }
  }

  public onSend(): void {
    this.buttonState = ButtonState.Sending;
    auth.sendPasswordResetEmail(this.email)
      .then(() => {
        this.$router.push('/log-in');
        sessionStorage.setItem('continueUrl', '');
      }, () => {
        this.email = '';
        this.buttonState = ButtonState.Error;
      });
  }

  public mounted(): void {
    const email = this.$refs.email as HTMLInputElement;
    email.focus();
  }
}
