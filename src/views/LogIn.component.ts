import { Component, Vue, Watch } from 'vue-property-decorator';
import { Action } from 'vuex-class';
import { auth } from '@/firebase.config';
import { Auth } from '@/store/modules/auth/auth.types';

enum ButtonState {
  LogIn,
  LoggingIn,
  Error,
}

@Component({})
export default class LogIn extends Vue {
  public email: string = '';
  public password: string = '';
  public buttonState: ButtonState = ButtonState.LogIn;

  @Action('set', { namespace: 'auth' })
  public setAuth!: (payload: Auth) => Promise<void>;

  get backUrl(): string {
    return localStorage.getItem('backUrl') || '/';
  }

  get buttonText(): string {
    switch (this.buttonState) {
      case ButtonState.LogIn:
        return 'Log in';

      case ButtonState.LoggingIn:
        return 'Logging in';

      case ButtonState.Error:
        return 'Please try again';
    }
  }

  get buttonClass(): string {
    switch (this.buttonState) {
      case ButtonState.LogIn:
        return 'btn-outline-primary';

      case ButtonState.LoggingIn:
        return 'btn-primary';

      case ButtonState.Error:
        return 'btn-danger';
    }
  }

  public onLogIn(): void {
    this.buttonState = ButtonState.LoggingIn;
    
    auth.signInWithEmailAndPassword(this.email, this.password)
      .then((userCredential: firebase.auth.UserCredential) => {
        if (userCredential.user) {
          this.setAuth({
            uid: userCredential.user.uid,
            email: userCredential.user.email,
            displayName: userCredential.user.displayName,
          });

          this.$router.push(sessionStorage.getItem('continueUrl') || '/');
          sessionStorage.setItem('continueUrl', '');
        } 
        
        else {
          this.email = '';
          this.password = '';
          this.buttonState = ButtonState.Error;
        }
      }, () => {
        this.email = '';
        this.password = '';
        this.buttonState = ButtonState.Error;
      });
  }

  public mounted(): void {
    const email = this.$refs.email as HTMLInputElement;
    email.focus();
  }
}
