import { Component, Vue } from 'vue-property-decorator';
import { Action } from 'vuex-class';
import { auth } from '@/firebase.config';
import RouteConfigs from '@/routes/routes.config';

@Component({})
export default class LogOut extends Vue {
  @Action('unset', { namespace: 'auth' })
  public unsetAuth!: () => void;

  get backUrl(): string {
    const backUrl = sessionStorage.getItem('continueUrl');
    return `${ (backUrl || '').length > 0 ? backUrl : '/' }`;
  }

  public mounted(): void {
    auth.signOut().then(() => {
      this.unsetAuth();
      setTimeout(() => {
        if (this.$route.name === RouteConfigs.LogOut.name) {
          this.$router.push(this.backUrl);
        }
      }, 5000);
    });
  }

}
