import { Component, Vue } from 'vue-property-decorator';
import { Action, Getter } from 'vuex-class';
import MapService from '../services/map.service';
import ImageService from '../services/image.service';
import ProceduralGenerationService from '../services/proceduralgeneration.service';
import { Cursor } from '../enums/cursor.enum';
import { KeyboardCode } from '@/enums/keyboardcode.enum';
import * as configuration from '../assets/configuration.json';
import { auth } from '@/firebase.config';
import RouteConfigs from '@/routes/routes.config';

@Component({})
export default class Home extends Vue {
  private tileW: number = 256;
  private tileH: number = 256;
  private canvasWidth: number = 0;
  private canvasHeight: number = 0;
  private x: number = 0;
  private y: number = 0;
  private cursorX: number = 0;
  private cursorY: number = 0;
  private alertMessages: string[] = [];
  private currentTile: any | null = null;
  private currentMonth: number = 0;
  private currentGrapeYield: number = 0;
  private currentCoins: number = 500;
  private currentDollars: number = 5;
  private currentTick: number = 0;
  private currentSeed: string = "";
  private currentCursor: Cursor = Cursor.None;
  private isFullScreen: boolean = false;

  @Getter('state', { namespace: 'state' })
  public state!: any;

  @Action('add', { namespace: 'state' })
  public add!: (payload: any) => Promise<void>;

  @Action('get', { namespace: 'state' })
  public get!: (payload: any) => Promise<void>;

  @Action('update', { namespace: 'state' })
  public update!: (payload: any) => Promise<void>;

  get gameContainer(): HTMLDivElement {
    return this.$refs.gameContainer as HTMLDivElement;
  }

  get canvas(): HTMLCanvasElement {
    return this.$refs.canvas as HTMLCanvasElement;
  }

  get context(): CanvasRenderingContext2D | null {
    return this.canvas.getContext("2d");
  }

  get canvasContainerDimensions(): object {
    return {
      width: `${this.canvasWidth}px`,
      height: `${this.canvasHeight}px`
    };
  }

  get panelDimensions(): object {
    return {
      right: "-2px",
      top: "0",
      height: `${this.canvas.offsetHeight}px`,
      minWidth: "400px",
      width: `${this.canvas.offsetWidth / 3}px`,
      zIndex: "1000"
    };
  }

  get grapeVarieties(): object[] {
    return configuration.grapeVarieties;
  }

  get terrainTypes(): string[] {
    return configuration.terrainTypes;
  }

  get monthNames(): string[] {
    return configuration.monthNames;
  }

  get buildings(): string[] {
    return configuration.buildings;
  }

  public onCanvasClick(event: MouseEvent): void {
    const rect = this.canvas.getBoundingClientRect();
    const x = event.clientX - rect.left;
    const y = event.clientY - rect.top;
    const closestTile = MapService.findClosestTile(x, y);
    if (closestTile === null) {
      return;
    }
    switch(this.currentCursor) {
      case Cursor.Winery:
        closestTile.isWinery = true;
        // todo: process purchase...
        break;
      default:
        this.currentTile = closestTile;
        break;
    }
  }

  public onCanvasMouseMove(event: MouseEvent): void {
    const rect = this.canvas.getBoundingClientRect();
    this.cursorX = event.clientX - rect.left;
    this.cursorY = event.clientY - rect.top;
  }

  public onCanvasKeyUp(event: KeyboardEvent): void {
    const inc = 128;
    switch (event.code) {
      case KeyboardCode.ArrowUp:
        this.y += inc;
        break;
      case KeyboardCode.ArrowDown:
        this.y -= inc;
        break;
      case KeyboardCode.ArrowLeft:
        this.x += inc;
        break;
      case KeyboardCode.ArrowRight:
        this.x -= inc;
        break;
    }
  }

  public onCanvasGreyOutClick(): void {
    this.currentTile = null;
  }

  public onTileFormSubmit(): void {
    // todo: immutable data
    // todo: process purchases
    if (this.currentTile.terrainType === 0) {
      this.currentTile.elevation = 0;
      this.currentTile.grapeVariety = 0;
    }
    this.currentTile = null;
  }

  public onRotateClick(): void {
    MapService.rotateMap();
  }

  public onGenerateClick(): void {
    this.generateMap();
  }

  public onSaveClick(): void {
    if (!auth.currentUser) {
      this.$router.push({ name: RouteConfigs.LogIn.name });
      return;
    }

    const mapJSON = MapService.exportStateJSON();
    const state = {
      mapJSON: mapJSON,
      currentMonth: this.currentMonth,
      currentGrapeYield: this.currentGrapeYield
    };

    if (this.state) {
      this.update({
        id: auth.currentUser.uid,
        fields: state,
      });
    } 
    
    else {
      this.add({
        id: auth.currentUser.uid,
        data: state,
      });
    }

    this.alertMessages.push("Game was saved!");
  }

  public onFullScreenClick(): void {
    if (!document.fullscreenElement) {
      this.gameContainer.requestFullscreen({
        navigationUI: "hide"
      });
    } 
    
    else {
      document.exitFullscreen();
    }
  }

  public onFullScreenChange(): void {
    this.setCanvasDimensions();
    this.isFullScreen = !!document.fullscreenElement;
  }

  public onBuildingClick(building: string): void {
    let cursor: Cursor;

    switch(building.toLowerCase()) {
      case "winery":
        cursor = Cursor.Winery;
        break;

      default:
        cursor = Cursor.None;
    }

    this.currentCursor = this.currentCursor === cursor ? Cursor.None : cursor;
  }

  public mounted(): void {
    this.setCanvasDimensions();

    ImageService.loadImages().then(() => {
      if (!auth.currentUser) {
        this.$router.push({ name: RouteConfigs.LogIn.name });
        return;
      }

      this.get(auth.currentUser.uid).then(() => {
        if (this.state) {
          this.currentMonth = this.state.currentMonth;
          this.currentGrapeYield = this.state.currentGrapeYield;
          MapService.importStateJSON(this.state.mapJSON);
        } 
        
        else {
          this.generateMap();
        }

        window.requestAnimationFrame(this.renderMap);
      });
    });

    document.addEventListener("fullscreenchange", this.onFullScreenChange);
    document.addEventListener("keyup", this.onCanvasKeyUp);
  }

  private incrementMonth(): void {
    this.alertMessages = [];
    this.currentMonth = (this.currentMonth + 1) % 12;

    if (this.currentMonth > 2 && this.currentMonth < 9) {
      MapService.updateVineStatus(false);
    }

    if (this.currentMonth === 9) {
      const vineCount = MapService.getVineCount();
      const grapeYield = vineCount * 2000;
      this.currentGrapeYield += grapeYield;
      this.alertMessages.push(`Harvest Time! ${grapeYield} grapes were harvested.`);
      MapService.updateVineStatus(true);
    }
  }

  private setCanvasDimensions(): void {
    this.canvasWidth = window.innerWidth;
    this.canvasHeight = window.innerHeight;
    this.canvas.width = this.canvasWidth;
    this.canvas.height = this.canvasHeight;
  }

  private generateMap(): void {
    ProceduralGenerationService.initPRNG();
    const seed = ProceduralGenerationService.getSeed();
    MapService.generateMap();
    this.currentSeed = seed;
    this.currentMonth = 0;
    this.currentGrapeYield = 0;
    this.alertMessages = [];
  }

  private renderMap(): void {
    const incrementMonthAt = (60 * 5);

    if (this.currentTick >= incrementMonthAt) {
      this.incrementMonth();
      this.currentTick = 0;
    }

    if (!this.context) {
      return;
    }
    
    MapService.renderMap(this.context, this.canvasWidth, this.canvasHeight, this.tileW, this.tileH, this.x, this.y, this.cursorX, this.cursorY, this.currentCursor);
    this.currentTick++;
    window.requestAnimationFrame(this.renderMap);
  }
}
