import { Route } from 'vue-router';

class RouteUtils {
  public static buildGuardFrom = (subGuards: Array<(to: Route, from: Route, next: any) => void>) => {
    return (to: Route, from: Route, next: any) => {
      subGuards.forEach((value: (to: Route, from: Route, next: any) => void) => {
        value(to, from, next);
      });
    };
  }

  public static buildGlobalGuardFrom = (subGuards: Array<(to: Route, from: Route) => void>) => {
    return (to: Route, from: Route) => {
      subGuards.forEach((value: (to: Route, from: Route) => void) => {
        value(to, from);
      });
    };
  }
}

export default RouteUtils;
