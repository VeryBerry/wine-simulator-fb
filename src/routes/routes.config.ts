import { RouteConfig } from 'vue-router';
import Home from '@/views/Home.vue';
import LogIn from '@/views/LogIn.vue';
import LogOut from '@/views/LogOut.vue';
import ForgottenPassword from '@/views/ForgottenPassword.vue';
import PageNotFound from '@/views/PageNotFound.vue';
import RouteGuards from './routes.guards';

class RouteConfigs {

  public static Home: RouteConfig =
  {
    path: '/',
    name: 'Home',
    component: Home,
    beforeEnter: RouteGuards.redirectToLogin,
  };

  public static LogIn: RouteConfig =
  {
    path: '/log-in',
    name: 'LogIn',
    component: LogIn,
  };

  public static LogOut: RouteConfig =
  {
    path: '/log-out',
    name: 'LogOut',
    component: LogOut,
  };

  public static ForgottenPassword: RouteConfig =
  {
    path: '/forgotten-password',
    name: 'ForgottenPassword',
    component: ForgottenPassword,
  };

  public static PageNotFound: RouteConfig =
  {
    path: '/page-not-found',
    name: 'PageNotFound',
    component: PageNotFound,
  };
  
}

export default RouteConfigs;
