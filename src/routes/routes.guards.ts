import { Route } from 'vue-router';
import { auth } from '@/firebase.config';

const authRoutes: Array<(string | undefined)> = [
  'LogIn',
  'LogOut',
  'ForgottenPassword',
];

class RouteGuards {
  public static redirectToLogin: (to: Route, from: Route, next: any ) => void =
    (to: Route, from: Route, next: any ) => {
      if (!auth.currentUser) {
        sessionStorage.setItem('continueUrl', to.path);
        next({ name: 'LogIn' });
      } 
      else {
        next();
      }
    }

  public static setBackUrl: (to: Route, from: Route) => void =
    (to: Route, from: Route) => {
      if (authRoutes.indexOf(from.name || '') < 0) {
        sessionStorage.setItem('backUrl', from.path);
      }

      const continueUrl = sessionStorage.getItem('continueUrl');
      const backUrl = sessionStorage.getItem('backUrl');

      if (continueUrl === backUrl) {
        sessionStorage.setItem('backUrl', '');
      }
    }

  public static setContinuePath: (to: Route, from: Route) => void =
    (to: Route, from: Route) => {
      if (authRoutes.indexOf(from.name || '') > -1) {
        const continueUrl = sessionStorage.getItem('continueUrl');

        if (continueUrl === to.path) {
          sessionStorage.setItem('continueUrl', '');
        }
      } 
      else if (authRoutes.indexOf(to.name || '') < 0) {
        sessionStorage.setItem('continueUrl', to.path);
      } 
      else {
        sessionStorage.setItem('continueUrl', from.path);
      }
    }
}

export default RouteGuards;
