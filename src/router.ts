import Vue from 'vue';
import Router, { Route } from 'vue-router';
import RouteConfigs from './routes/routes.config';
import RouteUtils from './routes/routes.utils';
import RouteGuards from './routes/routes.guards';

Vue.use(Router);

const router: Router = new Router({
  routes: [
    RouteConfigs.Home,
    RouteConfigs.LogIn,
    RouteConfigs.LogOut,
    RouteConfigs.ForgottenPassword,
    RouteConfigs.PageNotFound,
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    // },
    {
      path: '*',
      redirect: RouteConfigs.PageNotFound.path,
    },
  ],
  scrollBehavior: (to: Route, from: Route, savedPosition) => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  },
  mode: 'history',
});

router.afterEach(RouteUtils.buildGlobalGuardFrom([
  // RouteGuards.setLastSearchPath,
  RouteGuards.setBackUrl,
  RouteGuards.setContinuePath,
]));

export default router;
