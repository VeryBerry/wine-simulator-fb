import ImageService, { ImageType } from './image.service';
import RenderService from './render.service';
import TileService from './tile.service';
import ProceduralGenerationService from './proceduralgeneration.service';
import { Cursor } from '@/enums/cursor.enum';

let map: Tile[][] = [];

interface Tile {
  dxdy: string,
  i: number,
  j: number,
  center: { x: number, y: number },
  terrainType: number;
  isForested: boolean;
  isWinery: boolean;
  elevation: number;
  grapeVariety: number;
  vineStatus: number;
}

interface MapConfig {
  width: number;
  height: number;
}

const mapSize = 20;

class MapService {

  public get config(): MapConfig {
    return {
      width: mapSize,
      height: mapSize
    };
  }

  public generateMap(): void {
    // initialise
    for (let i = 0; i < mapSize; i++) {
      map[i] = [];

      for (let j = 0; j < mapSize; j++) {
        map[i][j] = { i: i, j: j, center: { x: 0, y: 0 }, dxdy: "", terrainType: 1, isForested: false, isWinery: false, elevation: 0, grapeVariety: 0, vineStatus: 0 };
      }
    }

    for (let c = 0; c < 24; c++) {
      // randomise terrain
      for (let i = 1; i < 4; i++) {
        const x = Math.floor(ProceduralGenerationService.random() * mapSize);
        const y = Math.floor(ProceduralGenerationService.random() * mapSize);
        const tiles = this.getTileSelection(map[x][y]);

        for (let t of tiles) {
          const neighbours = this.getNeighbouringTiles(t);

          for (let n of neighbours) {
            n.terrainType = i;
            n.isForested = i === 1 && c % 2 === 1;
          }

          t.terrainType = i;
          t.isForested = i === 1 && c % 2 === 1;
        }
      }
    }

    for (let c = 0; c < 4; c++) {
      // randomise water
      const x = Math.floor(ProceduralGenerationService.random() * mapSize);
      const y = Math.floor(ProceduralGenerationService.random() * mapSize);
      const tiles = this.getTileSelection(map[x][y]);

      for (let t of tiles) {
        const neighbours = this.getNeighbouringTiles(t);

        for (let n of neighbours) {
          n.terrainType = 0;
        }

        t.terrainType = 0;
      }
    }

    // randomise elevation
    for (let c = 0; c < 16; c++) {
      const x = Math.floor(ProceduralGenerationService.random() * mapSize);
      const y = Math.floor(ProceduralGenerationService.random() * mapSize);
      const tile = map[x][y];
      const tiles = this.getTileSelection(tile);

      for (let t of tiles) {
        const neighbours = this.getNeighbouringTiles(t);

        for (let n of neighbours) {
          if (n.terrainType > 0 && n.elevation < 2) {
            n.elevation = 1;
          }
        }

        if (t.terrainType > 0) {
          t.elevation = 2;
        }
      }
    }
  }

  public renderMap(ctx: CanvasRenderingContext2D, canvasWidth: number, canvasHeight: number, tileW: number, tileH: number, x = 0, y = 0, cursorX: number, cursorY: number, cursor: Cursor): void {   
    const mapX = tileW * 2.5 + x;
    const mapY = (tileH / 4) + y;

    RenderService.renderBackground(ctx, canvasWidth, canvasHeight);

    const closestTile = this.findClosestTile(cursorX, cursorY);

    for (var i = 0; i < map.length; i++) {
      for (var j = 0; j < map[i].length; j++) {
        const tile = map[i][j];
        const elevation = tile.terrainType > 0 ? tile.elevation * 32 : 0;
        const dx = (i - j) * (tileW / 2) + mapX;
        const dy = (i + j) * (tileH / 4) + mapY;

        tile.center.x = dx + (tileW / 2);
        tile.center.y = dy - elevation + (tileH / 4);
        tile.dxdy = `(${i - j}, ${i + j})`;

        // render dirt base
        RenderService.renderDirt(ctx, dx, dy, -1);

        // render tile terrain
        switch(tile.terrainType) {
          case 0:
            RenderService.renderWater(ctx, dx, dy);
            break;

          default:
            RenderService.renderDirt(ctx, dx, dy, 0);

            if (tile.elevation > 0) {
              RenderService.renderDirt(ctx, dx, dy, tile.elevation);
            }

            if (tile.isForested) {
              RenderService.renderForest(ctx, dx, dy - elevation);
            }

            else if (tile.isWinery) {
              RenderService.renderWinery(ctx, dx, dy - elevation);
            }
            
            else {
              const imageType = tile.terrainType + 3 as ImageType;
              RenderService.renderTile(ctx, ImageService.getImage(imageType), dx, dy - elevation - 112);

              if (tile.grapeVariety > 0 && tile.vineStatus > 0) {
                RenderService.renderVine(ctx, dx, dy - elevation - 112, tile.vineStatus);
              }
            }

            break;
        }

        // render cursor tile
        const isHover = closestTile !== null && closestTile.i === tile.i && closestTile.j === tile.j;

        if (isHover) {
          if (cursor === Cursor.Winery) {
            RenderService.renderWinery(ctx, dx, dy - elevation);
          }

          RenderService.renderHover(ctx, dx, dy - elevation);
        }
      }
    }
  }

  public rotateMap(): void {
    let result: Tile[][] = [];
    const size = map.length;

    for (let j = 0; j < size; j++) {
      result[j] = [];

      for (let i = 0; i < size ; i++) {
        result[j][i] = map[size - i - 1][j];
      }
    }

    map = result;
  }

  public findClosestTile(x: number, y: number): Tile | null {
    for (let i = 0; i < map.length; i++) {
      for (let j = 0; j < map[i].length; j++) {
        const t = map[i][j];
        const d = Math.sqrt(Math.pow(t.center.x - x, 2) + Math.pow(t.center.y - y, 2));

        if (d < 56) {
          return t;
        }
      }
    }

    return null;
  }

  public getNeighbouringTiles(tile: Tile): Tile[] {
    let tiles = [];

    for (let i = -1; i <= 1; i++) {
      const mi = map[tile.i+i];

      if (mi) {
        for (let j = -1; j <= 1; j++) {
          if (i === 0 && j === 0) {
            continue;
          }

          const mj = mi[tile.j+j];

          if (mj) {
            tiles.push(mj);
          }
        }
      }
    }
    return tiles;
  }

  public getVineCount(): number {
    let count = 0;

    for (let i = 0; i < map.length; i++) {
      for (let j = 0; j < map[i].length; j++) {
        if (map[i][j].grapeVariety > 0) {
          count++;
        }
      }
    }

    return count;
  }

  public updateVineStatus(reset: boolean): void {
    for (let i = 0; i < map.length; i++) {
      for (let j = 0; j < map[i].length; j++) {
        const t = map[i][j];

        if (t.grapeVariety > 0) {
          if (reset) {
            t.vineStatus = 0;
          }
          
          else {
            t.vineStatus = t.vineStatus < 4 ? t.vineStatus + 1 : t.vineStatus;
          }
        }
      }
    }
  }

  public exportStateJSON(): string {
    return JSON.stringify(map);
  }

  public importStateJSON(state: string): void {
    map = JSON.parse(state);
  }

  private getTileSelection(tile: Tile): Tile[] {
    const s = Math.floor(ProceduralGenerationService.random() * 5);
    const r = Math.floor(ProceduralGenerationService.random() * 4);
    let tr: { x: number, y: number }[] = [];

    switch (s) {
      case 0:
        tr =  TileService.getLine(r);
        break;

      case 1:
        tr =  TileService.getBox(r);
        break;

      case 2:
        tr =  TileService.getShapeOne(r);
        break;

      case 3:
        tr =  TileService.getShapeTwo(r);
        break;

      case 4:
        tr = TileService.getShapeThree(r);
        break;
    }

    let result = [];

    for (let t of tr) {
      const mi = map[tile.i + t.x];

      if (mi) {
        const mj = mi[tile.j + t.y];

        if (mj) {
          result.push(mj);
        }
      }
    }
    
    return result;
  }
}

export default new MapService();