let seed: string = "";
let PRNG: () => number = () => 0;

// hash computation
const xmur3 = (str: string): () => number => {
  let h = 1779033703 ^ str.length;

  for (let i = 0; i < str.length; i++) {
    h = Math.imul(h ^ str.charCodeAt(i), 3432918353);
    h = h << 13 | h >>> 19;
  }

  return (): number => {
    h = Math.imul(h ^ h >>> 16, 2246822507);
    h = Math.imul(h ^ h >>> 13, 3266489909);

    return (h ^= h >>> 16) >>> 0;
  };
}

// PRNG
const sfc32 = (a: number, b: number, c: number, d: number): () => number => {
  return (): number => {
    a >>>= 0; b >>>= 0; c >>>= 0; d >>>= 0; 

    let t = (a + b) | 0;
    a = b ^ b >>> 9;
    b = c + (c << 3) | 0;
    c = (c << 21 | c >>> 11);
    d = d + 1 | 0;
    t = t + d | 0;
    c = c + t | 0;

    return (t >>> 0) / 4294967296;
  }
}

class ProceduralGenerationService {
  public getSeed(): string {
    return seed;
  }

  public generateSeed(length: number): void {
    const hexValues: string[] = [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" ];
    let newSeed: string = "";

    for (let i = 0; i < length; i++) {
      const h = Math.floor(Math.random() * hexValues.length);
      newSeed += hexValues[h];
    }

    seed = newSeed;
  }

  public initPRNG(): void {
    this.generateSeed(64);
    const hash = xmur3(seed);
    PRNG = sfc32(hash(), hash(), hash(), hash());
  }
  
  public random(): number {
    return PRNG();
  }
}

export default new ProceduralGenerationService();