const tileImagesToLoad = [
  "iso_dirt.png",
  "iso_dirt_el_1.png",
  "iso_dirt_el_2.png",
  "iso_water.png",
  "iso_greenway.png",
  "iso_clay.png",
  "iso_gravel.png",
  "iso_forest.png",
  "iso_vine_3.png",
  "iso_tile.png",
  "iso_winery.png"
];

let tileImages: HTMLImageElement[] = [];

export enum ImageType {
  Dirt,
  Dirt_El_1,
  Dirt_El_2,
  Water,
  Greenway,
  Clay,
  Gravel,
  Forest,
  Vine_3,
  Tile,
  Winery,
}

class ImageService {

  public loadImages(): Promise<void> {
    return new Promise<void>((resolve) => {
      let tileImagesLoaded = 0;

      for (var i = 0; i < tileImagesToLoad.length; i++) {
        tileImages[i] = new Image();
        tileImages[i].src = `/img/tiles/${tileImagesToLoad[i]}`;
        tileImages[i].onload = () => {
          tileImagesLoaded++;

          if (tileImagesLoaded === tileImagesToLoad.length) {
            resolve();
          }
        }
      }
    });
  }

  public getImages(): HTMLImageElement[] {
    return tileImages;
  }

  public getImage(type: ImageType): HTMLImageElement {
    if (tileImages.length > type) {
      return tileImages[type];
    }

    return new HTMLImageElement();
  }
  
}

export default new ImageService();