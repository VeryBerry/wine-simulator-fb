import ImageService, { ImageType } from './image.service';

class RenderService {

  public renderBackground(ctx: CanvasRenderingContext2D, width: number, height: number): void {
    ctx.clearRect(0, 0, width, height);
    ctx.fillStyle = "#b3e5fc";
    ctx.fillRect(0, 0, width, height);
  }

  public renderTile(ctx: CanvasRenderingContext2D, tileImage: HTMLImageElement, x: number, y: number): void {
    ctx.drawImage(tileImage, x, y);
  }

  public renderHover(ctx: CanvasRenderingContext2D, x: number, y: number): void {
    ctx.drawImage(ImageService.getImage(ImageType.Tile), x, y - 112);
  }

  public renderDirt(ctx: CanvasRenderingContext2D, x: number, y: number, elevation: number): void {
    switch (elevation) {
      case -1:
        ctx.drawImage(ImageService.getImage(ImageType.Dirt), x, y + 124);
        break;

      case 1:
        ctx.drawImage(ImageService.getImage(ImageType.Dirt_El_1), x, y - 124);
        break;

      case 2:
        ctx.drawImage(ImageService.getImage(ImageType.Dirt_El_2), x, y - 124);
        break;
        
      default:
        ctx.drawImage(ImageService.getImage(ImageType.Dirt), x, y);
        break;
    }
  }

  public renderWater(ctx: CanvasRenderingContext2D, x: number, y: number): void {
    ctx.drawImage(ImageService.getImage(ImageType.Water), x, y);
  }

  public renderVine(ctx: CanvasRenderingContext2D, x: number, y: number, vineStatus: number): void {
    ctx.drawImage(ImageService.getImage(ImageType.Vine_3), x, y);
  }

  public renderForest(ctx: CanvasRenderingContext2D, x: number, y: number): void {
    ctx.drawImage(ImageService.getImage(ImageType.Forest), x, y - 112);
  }

  public renderWinery(ctx: CanvasRenderingContext2D, x: number, y: number): void {
    ctx.drawImage(ImageService.getImage(ImageType.Winery), x, y - 112);
  }

  public renderCenter(ctx: CanvasRenderingContext2D, center: { x: number, y: number }, i: number, j: number): void {
    ctx.beginPath();
    ctx.arc(center.x, center.y, 1, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.font = '16px serif';
    ctx.fillText(`(${i - j}, ${i + j})`, center.x + 4, center.y);
  }
}

export default new RenderService();