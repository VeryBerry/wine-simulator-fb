class TileService {

  public getLine(v: number): { x: number, y: number }[] {
    switch (v) {
      case 0:
        return [
          { x: 0, y: 0 },
          { x: -1, y: 0 },
          { x: -2, y: 0 },
          { x: -3, y: 0 }
        ];

      case 1:
        return [
          { x: 0, y: 0 },
          { x: 1, y: 0 },
          { x: 2, y: 0 },
          { x: 3, y: 0 }
        ];

      case 2:
        return [
          { x: 0, y: 0 },
          { x: 0, y: -1 },
          { x: 0, y: -2 },
          { x: 0, y: -3 }
        ];

      case 3:
        return [
          { x: 0, y: 0 },
          { x: 0, y: 1 },
          { x: 0, y: 2 },
          { x: 0, y: 3 }
        ];
    }

    return [];
  }

  public getBox(v: number): { x: number, y: number }[] {
    switch (v) {
      case 0:
        return [
          { x: 0, y: 0 },
          { x: -1, y: 0 },
          { x: 0, y: -1 },
          { x: -1, y: -1 }
        ];

      case 1:
        return [
          { x: 0, y: 0 },
          { x: -1, y: 0 },
          { x: 0, y: 1 },
          { x: -1, y: 1 }
        ];

      case 2:
        return [
          { x: 0, y: 0 },
          { x: 1, y: 0 },
          { x: 0, y: -1 },
          { x: 1, y: -1 }
        ];

      case 3:
        return [
          { x: 0, y: 0 },
          { x: 1, y: 0 },
          { x: 0, y: 1 },
          { x: 1, y: 1 }
        ];
    }

    return [];
  }

  public getShapeOne(v: number): { x: number, y: number }[] {
    switch (v) {
      case 0:
        return [
          { x: 0, y: 0 },
          { x: -1, y: 0 },
          { x: -2, y: 0 },
          { x: -2, y: -1 }
        ];

      case 1:
        return [
          { x: 0, y: 0 },
          { x: 1, y: 0 },
          { x: 2, y: 0 },
          { x: 2, y: 1 }
        ];

      case 2:
        return [
          { x: 0, y: 0 },
          { x: 0, y: -1 },
          { x: 0, y: -2 },
          { x: -1, y: -2 }
        ];

      case 3:
        return [
          { x: 0, y: 0 },
          { x: 0, y: 1 },
          { x: 0, y: 2 },
          { x: 1, y: 2 }
        ];
    }

    return [];
  }

  public getShapeTwo(v: number): { x: number, y: number }[] {
    switch (v) {
      case 0:
        return [
          { x: 0, y: 0 },
          { x: -1, y: 0 },
          { x: -1, y: -1 },
          { x: -2, y: -1 }
        ];

      case 1:
        return [
          { x: 0, y: 0 },
          { x: 1, y: 0 },
          { x: 1, y: 1 },
          { x: 2, y: 1 }
        ];

      case 2:
        return [
          { x: 0, y: 0 },
          { x: 0, y: -1 },
          { x: -1, y: -1 },
          { x: -1, y: -2 }
        ];

      case 3:
        return [
          { x: 0, y: 0 },
          { x: 0, y: 1 },
          { x: 1, y: 1 },
          { x: 1, y: 2 }
        ];
    }

    return [];
  }

  public getShapeThree(v: number): { x: number, y: number }[] {
    switch (v) {
      case 0:
        return [
          { x: 0, y: 0 },
          { x: -1, y: 0 },
          { x: -1, y: -1 },
          { x: -2, y: 0 }
        ];

      case 1:
        return [
          { x: 0, y: 0 },
          { x: 1, y: 0 },
          { x: 1, y: 1 },
          { x: 2, y: 0 }
        ];

      case 2:
        return [
          { x: 0, y: 0 },
          { x: 0, y: -1 },
          { x: -1, y: -1 },
          { x: 0, y: -2 }
        ];

      case 3:
        return [
          { x: 0, y: 0 },
          { x: 0, y: 1 },
          { x: 1, y: 1 },
          { x: 0, y: 2 }
        ];
    }
    
    return [];
  }
}

export default new TileService();